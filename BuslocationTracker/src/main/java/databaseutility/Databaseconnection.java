package databaseutility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Databaseconnection {
	
	private static Databaseconnection INSTANCE ;
	private static Connection con = null;
	
	private Databaseconnection() {
		String url = "jdbc:mysql://localhost:3306/user_Information?allowPublicKeyRetrieval=true&useSSL=false";
		String username = "root";
		String pass = "";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
//			Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection(url,username,pass);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{ 
            System.out.println("finally block executed"); 
        }
	}
	public static Connection getInstance() {
		if(INSTANCE == null && con == null) {
			INSTANCE = new Databaseconnection();
		}
		
		return INSTANCE.con;

	}
	
}
