package flightDetail;

import org.codehaus.jackson.annotate.JsonProperty;

public class Provider {
	 private String one;
	    private String two;
	    private String three;
	    private String four;

	    @JsonProperty("1")
	    public String getOne() {
	        return one;
	    }

	    public void setOne(String one) {
	        this.one = one;
	    }
	    @JsonProperty("2")
	    public String getTwo() {
	        return two;
	    }

	    public void setTwo(String two) {
	        this.two = two;
	    }
	    @JsonProperty("3")
	    public String getThree() {
	        return three;
	    }

	    public void setThree(String three) {
	        this.three = three;
	    }
	    @JsonProperty("4")
	    public String getFour() {
	        return four;
	    }

	    public void setFour(String four) {
	        this.four = four;
	    }

}
