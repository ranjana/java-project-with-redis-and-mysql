package flightDetail;

import org.codehaus.jackson.annotate.JsonProperty;

public class Appendix {

	
	    private Airlines airlines;
	    private Airports airports;
	    private Provider providers;

	    @JsonProperty("airlines")
	    public Airlines getAirlines() {
	        return airlines;
	    }
	    public void setAirlines(Airlines airlines) {
	        this.airlines = airlines;
	    }
	    @JsonProperty("airports")
	    public Airports getAirports() {
	        return airports;
	    }

	    public void setAirports(Airports airports) {
	        this.airports = airports;
	    }
	    @JsonProperty("providers")
	    public Provider getProviders() {
	        return providers;
	    }

	    public void setProviders(Provider providers) {
	        this.providers = providers;
	    }

	    
}
