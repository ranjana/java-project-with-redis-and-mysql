package flightDetail;

import org.codehaus.jackson.annotate.JsonProperty;

public class Airports {
	  private String DEL;
	    private String BOM;

	    @JsonProperty("DEL")
	    public String getDEL() {
	        return DEL;
	    }

	    public void setDEL(String DEL) {
	        this.DEL = DEL;
	    }
	    @JsonProperty("BOM")
	    public String getBOM() {
	        return BOM;
	    }

	    public void setBOM(String BOM) {
	        this.BOM = BOM;
	    }

}
