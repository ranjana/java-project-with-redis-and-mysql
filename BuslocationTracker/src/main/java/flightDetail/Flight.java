package flightDetail;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class Flight {
	  private  String originCode;
	    private  String destinationCode;
	    private  long departureTime;
	    private  long arrivalTime;
	    private ArrayList<Fare> fares;
	    private  String airlineCode;
	    private String bookingClass;
	    @JsonProperty("originCode")
	    public String getOriginCode() {
	        return originCode;
	    }

	    public void setOriginCode(String originCode) {
	        this.originCode = originCode;
	    }
	    @JsonProperty("destinationCode")
	    public String getDestinationCode() {
	        return destinationCode;
	    }

	    public void setDestinationCode(String destinationCode) {
	        this.destinationCode = destinationCode;
	    }
	    @JsonProperty("departureTime")
	    public long getDepartureTime() {
	        return departureTime;
	    }

	    public void setDepartureTime(long departureTime) {
	        this.departureTime = departureTime;
	    }
	    @JsonProperty("arrivalTime")
	    public long getArrivalTime() {
	        return arrivalTime;
	    }

	    public void setArrivalTime(long arrivalTime) {
	        this.arrivalTime = arrivalTime;
	    }
	    @JsonProperty("fares")
	    public ArrayList<Fare> getFares() {
	        return fares;
	    }

	    public void setFares(ArrayList<Fare> fares) {
	        this.fares = fares;
	    }

	    @JsonProperty("airlineCode")
	    public String getAirlineCode() {
	        return airlineCode;
	    }

	    public void setAirlineCode(String airlineCode) {
	        this.airlineCode = airlineCode;
	    }
	    @JsonProperty("class")
	    public String getBookingClass() {
	        return bookingClass;
	    }
	    public void setBookingClass(String bookingClass) {
	        this.bookingClass = bookingClass;
	    }

}
