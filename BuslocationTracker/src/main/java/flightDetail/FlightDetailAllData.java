package flightDetail;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class FlightDetailAllData {

	
	 private Appendix appendix;
	    private ArrayList<Flight> flights;
	    @JsonProperty("appendix")
	    public Appendix getAppendix() {
	        return appendix;
	    }

	    public void setAppendix(Appendix appendix) {
	        this.appendix = appendix;
	    }
	    @JsonProperty("flights")
	    public ArrayList<Flight> getFlights() {
	        return flights;
	    }

	    public void setFlights(ArrayList<Flight> flights) {
	        this.flights = flights;
	    }
	    
}
