package redis;

import redis.clients.jedis.Jedis;

public class RedisConnection {
	
	private  static RedisConnection Instance =null;
	private Jedis jedis;
	
	private RedisConnection() {
		try {
			jedis = new Jedis("localhost");
			// prints out "Connection Successful" if Java successfully connects to Redis server.
			System.out.println("Connection Successful");
			System.out.println("The server is running " + jedis.ping());
//			jedis.set("company-name", "500Rockets.io");
//			System.out.println("Stored string in redis:: "+ jedis.get("company-name"));

			}catch(Exception e) {
			System.out.println(e);
			}
	}
	
	public static Jedis getInstance() {
		if(Instance == null) {
			Instance = new RedisConnection();
		}
		
		return Instance.jedis;
		
	}

}
