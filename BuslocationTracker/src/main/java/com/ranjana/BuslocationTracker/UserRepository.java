package com.ranjana.BuslocationTracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import flightDetail.Flight;
import flightDetail.FlightDetailAllData;
import redis.RedisConnection;
import redis.clients.jedis.Jedis;
public class UserRepository {

	List<User> users;
	Connection con = null;
	
	public UserRepository() {
//		String url = "jdbc:mysql://localhost:3306/user_Information?allowPublicKeyRetrieval=true&useSSL=false";
//		String username = "root";
//		String pass = "";
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
////			Class.forName("com.mysql.jdbc.Driver");
//				con = DriverManager.getConnection(url,username,pass);
//				if(con == null) {
//					System.out.println("mysql couldnt connected");
//				}
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}finally{ 
//            System.out.println("finally block executed"); 
//        }
	}
	public static List<User> test() {
		List<User> users = new ArrayList<User>();
		User u2 = new User();
		u2.setName("rakesh");
		u2.setSurname("roushan");
		u2.setId(102);
		users.add(u2);
		return users;
	}
	
	public List<User>  getAllusers(){
		List<User> users = new ArrayList<User>();
		String sql = "select * from users";
			try {
			Statement st= con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				User u = new User();
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				u.setSurname(rs.getNString(3));
				users.add(u);
				System.out.println("id is = "+rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return users;
	}
	
	public User getUserById(int id) {
		String sql = "select *  from users where id ="+id;
		System.out.println("start of getuser getUserById........");
		try {
			System.out.println("start of getuser try........");
			User u = new User();
			Statement st= con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			System.out.println("start of executeQuery........");
			if(rs.next()) {
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				u.setSurname(rs.getNString(3));
				return u;
		
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{ 
            System.out.println("finally block of get user by id executed"); 
        }
		
		return null;
	}

	public void createUser(User user) {
		String sql = "INSERT INTO users VALUES (?,?,?)"; 
	
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.setString(2,user.name);
			ps.setString(3,user.surname);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void insertStop() {
	    URL url = UserRepository.class.getClassLoader().getResource("stops.txt");	
		try {
			File openFile = new File(url.getPath());
			Scanner read = new Scanner(openFile);
			read.useDelimiter(",|\\n");
			String stop_id,stop_code,stop_name,stop_lat,stop_lon;
			String sql = "INSERT INTO stops VALUES (?,?,?,?,?)"; 
			PreparedStatement ps = con.prepareStatement(sql);
			while(read.hasNext())
			   {
				stop_id = read.next();
				stop_code = read.next();
				stop_name = read.next();
				stop_lat = read.next();
				stop_lon = read.next();
				ps.setInt(1,Integer.parseInt(stop_id));
				ps.setString(2,stop_code);
				ps.setString(3,stop_name);
				ps.setString(4,stop_lat);
				ps.setString(5,stop_lon);
				ps.executeUpdate();
			     System.out.println(stop_id + " " + stop_code + " " + stop_name + " " + stop_lat + " " + stop_lon + "\n"); 
			     //just for debugging
			   }
			   read.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addstopTimes() {
		 URL url = UserRepository.class.getClassLoader().getResource("stop_times.txt");	
			try {
				File openFile = new File(url.getPath());
				Scanner read = new Scanner(openFile);
				read.useDelimiter(",|\\n");
				String trip_id,arrival_time,departure_time,stop_id,stop_sequence;
				String sql = "INSERT INTO stop_times VALUES (default,?,?,?,?,?)"; 
				PreparedStatement ps = con.prepareStatement(sql);
				while(read.hasNext())
				   {
					trip_id = read.next();
					arrival_time = read.next();
					departure_time = read.next();
					stop_id = read.next();
					stop_sequence = read.next();
					ps.setString(1,trip_id);
					ps.setString(2,arrival_time);
					ps.setString(3,departure_time);
					ps.setString(4,stop_id);
					ps.setString(5,stop_sequence);
					ps.executeUpdate();
				     System.out.println(trip_id + " " + arrival_time + " " + departure_time + " " + stop_id + " " + stop_sequence + "\n"); 
				     //just for debugging
				   }
				   read.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void addTrips() {
		 URL url = UserRepository.class.getClassLoader().getResource("trips.txt");	
			try {
				File openFile = new File(url.getPath());
				Scanner read = new Scanner(openFile);
				read.useDelimiter(",|\\n");
				String route_id,service_id,trip_id,shape_id;
				String sql = "INSERT INTO trips VALUES (default,?,?,?,?)"; 
				PreparedStatement ps = con.prepareStatement(sql);
				while(read.hasNext())
				   {
					route_id = read.next();
					service_id = read.next();
					trip_id = read.next();
					shape_id = read.next();
					ps.setString(1,route_id);
					ps.setString(2,service_id);
					ps.setString(3,trip_id);
					ps.setString(4,shape_id);
					ps.executeUpdate();
				     System.out.println(route_id + " " + service_id + " " + trip_id + " " + shape_id + "\n"); 
				     //just for debugging
				   }
				   read.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	
	public String setConnection() {
	    String urllink = "http://www.mocky.io/v2/5979c6731100001e039edcb3";
		HttpConnection task = new HttpConnection();
		return task.setconnection(urllink);
	}
	public String  addflightDetails() {
		String jsonData = setConnection();
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<Flight> flights = new ArrayList<Flight>();
        try {
			FlightDetailAllData dummyClass= objectMapper.readValue(jsonData, FlightDetailAllData.class);
			  if(dummyClass != null){
				  flights = dummyClass.getFlights();
	            }
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return "flightdetails";
	}

public static void jadistest() {
	Jedis jedis = RedisConnection.getInstance();
	System.out.println(jedis.get("company-name"));
	}
}
