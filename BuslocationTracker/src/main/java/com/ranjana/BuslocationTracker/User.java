package com.ranjana.BuslocationTracker;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
	int pid;
	String name; 
	String surname;
	int id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		System.out.println(name);
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		System.out.println(surname);
		this.surname = surname;
	}
	@Override
	public String toString() {
		System.out.println("toString");

		return "User [name=" + name + ", surname=" + surname + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	} 
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	
}
