package com.ranjana.BuslocationTracker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpConnection {
	 String line;
	    String jsonData;
	    public  void jsonData(String jsonData){
	        this.jsonData =jsonData;
	    }

	    public String getJsonData(){
	        return jsonData;
	    }
	    protected String setconnection(String urllink) {
	        try {
	            URL url = new URL(urllink);
	            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	            connection.connect();
	            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	            StringBuffer buffer = new StringBuffer();
	            while ((line = reader.readLine()) != null) {
	                buffer.append(line);

	            }
	            return buffer.toString();
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return line;
	    }
}
