package requestResposnse;

public enum RequestStage {
	RUNNIG(1) {
		@Override
		public String getRequestStage() {
			return "running";
		}
	},
	NOT_RUNNING(2) {
		@Override
		public String getRequestStage() {
			return null;
		}
	};

	private int code;

	private RequestStage(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public  abstract String getRequestStage();
}
