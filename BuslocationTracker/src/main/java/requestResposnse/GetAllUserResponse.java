package requestResposnse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.ranjana.BuslocationTracker.User;

import databaseutility.Databaseconnection;
import redis.RedisConnection;
import redis.clients.jedis.Jedis;

@XmlRootElement
public class GetAllUserResponse extends AbstractResposne{
int responseType;
List<User>  users;
int reponseStatus;
	
GetAllUserResponse(){
//		this.responseType = responseType;	
	}

	@Override
	public GetAllUserResponse process() {
		return getAllusers();
	}
	
	public GetAllUserResponse  getAllusers(){
		GetAllUserResponse resposne = new GetAllUserResponse();
		List<User> users = new ArrayList<User>();
		String sql = "select * from users";
	    Connection con =	Databaseconnection.getInstance();
			try {
			Statement st= con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				User u = new User();
				u.setPid(rs.getInt(1));
				u.setId(rs.getInt(2));
				u.setName(rs.getString(3));
				u.setSurname(rs.getNString(4));
				users.add(u);
				System.out.println("id is = "+rs.getInt(1));
			}
			resposne.setUsers(users);
			resposne.setResponseType(1);
			resposne.setReponseStatus(ResposnseStatus.SUCCSESS.getCode());
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return resposne;
	}

	public int getResponseType() {
		return responseType;
	}

	public void setResponseType(int responseType) {
		this.responseType = responseType;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public int getReponseStatus() {
		return reponseStatus;
	}

	public void setReponseStatus(int reponseStatus) {
		this.reponseStatus = reponseStatus;
	}	

}
