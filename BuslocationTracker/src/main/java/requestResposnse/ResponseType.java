package requestResposnse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public enum ResponseType {

	   GET_ALL_USER(1) {
		@Override
		public String requestName() {
			return "get_all_user";
		}
	},
	    APIErrorTwo(2) {
			@Override
			 public String requestName() {
				return null;
			}
		},
	    APIErrorThree(3) {
			@Override
			public String requestName() {
				return null;
			}
		},
	    APIErrorFour(4) {
			@Override
			public String requestName() {
				return null;
			}
		};

	    private int code;

	    private ResponseType(int code) {
	        this.code = code;
	    }

	    public int getCode() {
	        return this.code;
	    }
	    
	  public  abstract String requestName();
	

}
