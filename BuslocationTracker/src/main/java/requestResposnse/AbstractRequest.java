package requestResposnse;

import redis.RedisConnection;
import redis.clients.jedis.Jedis;

public abstract class AbstractRequest implements RequestBase{
	int requestType;
	public AbstractRequest(int requestType){
		this.requestType = requestType;
	}

	@Override
	public boolean preProcess() {
		Jedis jedis = RedisConnection.getInstance();
		if(jedis.get("type") != null && !jedis.get("type").isEmpty()) {
			System.out.println(""+requestType +" is alreay in process");	
			return true;
		}else {
			jedis.set("type",RequestStage.RUNNIG.getRequestStage());
		}
		return false;
	
	}
}
