package requestResposnse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public enum ResposnseStatus {

	   SUCCSESS(1) {
		@Override
		public String ResposnseStatus() {
			return "success";
		}
	},
	    FAILUAR(2) {
			@Override
			 public String ResposnseStatus() {
				return "failed";
			}
		};

	    private int code;

	    private ResposnseStatus(int code) {
	        this.code = code;
	    }

	    public int getCode() {
	        return this.code;
	    }
	    
	  public  abstract String ResposnseStatus();
	

}
